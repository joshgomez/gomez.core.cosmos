﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.Core.Cosmos.Models.Areas.Identity;
using Gomez.Core.DocumentDb;
using System.Threading.Tasks;

namespace Gomez.Core.Cosmos.Repositories
{
    public interface IRefreshTokenRepository<TUser, TRole> : IDocumentRepository<RefreshTokens>
         where TRole : DocumentDbIdentityRole
         where TUser : DocumentDbIdentityUser<TRole>
    {
        Task<RefreshTokens> DeleteByTokenAsync(TUser user, string token);

        Task<RefreshToken> GetTokenAsync(TUser user, string token);

        Task RevokeAsync(TUser user);

        Task<RefreshTokens> SaveTokenAsync(TUser user, RefreshToken refreshToken);
    }
}