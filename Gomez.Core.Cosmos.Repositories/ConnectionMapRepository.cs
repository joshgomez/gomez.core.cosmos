﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.Core.Cosmos.Models.Areas.Sockets;
using Gomez.Core.DocumentDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.Cosmos.Repositories
{
    public class ConnectionMapRepository<TUser, TRole> : TypedDocumentRepository<ConnectionMap>, IConnectionMapRepository<TUser, TRole>
        where TRole : DocumentDbIdentityRole
        where TUser : DocumentDbIdentityUser<TRole>
    {
        private readonly string _hubName;

        public ConnectionMapRepository(
            IDocumentDbContext context,
            string collectionName,
            string hubName) : base(
                context,
                collectionName,
                x => x.PartitionKey,
                typeof(ConnectionMap).Name)
        {
            _hubName = hubName;
        }

        public async Task<int> CountAsync(bool? online)
        {
            var currentTime = DateTime.UtcNow;
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = _hubName
            });

            if (online == true)
            {
                return await _context.CountAsync(query
                    .Where(x => currentTime <= x.ExpireAt));
            }

            if (online == false)
            {
                return await _context.CountAsync(query
                    .Where(x => currentTime > x.ExpireAt));
            }

            return await _context.CountAsync(query);
        }

        public async Task<int> CountAsync(bool? online, string groupId)
        {
            var currentTime = DateTime.UtcNow;
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = _hubName
            });

            if (online == true)
            {
                return await _context.CountAsync(query
                    .Where(x => currentTime <= x.ExpireAt && x.Connections.Any(z => z.GroupId == groupId)));
            }

            if (online == false)
            {
                return await _context.CountAsync(query
                    .Where(x => currentTime > x.ExpireAt && x.Connections.Any(z => z.GroupId == groupId)));
            }

            return await _context.CountAsync(query.Where(x => x.Connections.Any(z => z.GroupId == groupId)));
        }

        public IAsyncEnumerable<string> GetExpiredUsersAsync()
        {
            var currentTime = DateTime.UtcNow;
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = _hubName
            }).Where(x => currentTime > x.ExpireAt)
                .Select(x => x.Id);

            return _context.ToAsyncEnumerable(query);
        }

        public IAsyncEnumerable<string> GetExpiredUsersAsync(string groupId)
        {
            var currentTime = DateTime.UtcNow;
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = _hubName
            }).Where(x => currentTime > x.ExpireAt && x.Connections.Any(z => z.GroupId == groupId))
                .Select(x => x.Id);

            return _context.ToAsyncEnumerable(query);
        }

        public IAsyncEnumerable<string> GetActiveUsersAsync()
        {
            var currentTime = DateTime.UtcNow;
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = _hubName
            }).Where(x => currentTime <= x.ExpireAt)
                .Select(x => x.Id);
            return _context.ToAsyncEnumerable(query);
        }

        public IAsyncEnumerable<string> GetActiveUsersAsync(string groupId)
        {
            var currentTime = DateTime.UtcNow;
            var query = GetQuery(new QueryOptions()
            {
                PartitionKey = _hubName
            }).Where(x => currentTime <= x.ExpireAt && x.Connections.Any(z => z.GroupId == groupId))
                .Select(x => x.Id);
            return _context.ToAsyncEnumerable(query);
        }
    }
}