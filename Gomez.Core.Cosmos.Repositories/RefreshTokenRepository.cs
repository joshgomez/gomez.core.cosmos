﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.Core.Cosmos.Models.Areas.Identity;
using Gomez.Core.DocumentDb;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.Cosmos.Repositories
{
    public class RefreshTokenRepository<TUser, TRole> : TypedDocumentRepository<RefreshTokens>, IRefreshTokenRepository<TUser, TRole>
        where TRole : DocumentDbIdentityRole
        where TUser : DocumentDbIdentityUser<TRole>
    {
        public RefreshTokenRepository(
            IDocumentDbContext context,
            string collectionName) : base(
                context,
                collectionName,
                x => x.PartitionKey,
                typeof(RefreshTokens).Name)
        {
        }

        private string GetId(TUser user)
        {
            return user.Id + "_tokens";
        }

        public async Task<RefreshTokens> DeleteByTokenAsync(TUser user, string token)
        {
            string id = GetId(user);
            var tokens = await ReadAsync(id, user.PartitionKey);
            if (tokens == null)
            {
                return null;
            }

            return await UpdateAsync(tokens, t =>
            {
                var tokenItem = t.Tokens.FirstOrDefault(x => x.Token == token);
                t.Tokens.Remove(tokenItem);
                return t;
            });
        }

        public async Task<RefreshTokens> SaveTokenAsync(TUser user, RefreshToken refreshToken)
        {
            string id = GetId(user);
            var tokens = await ReadAsync(id, user.PartitionKey);
            if (tokens == null)
            {
                tokens = new RefreshTokens(id) { PartitionKey = user.PartitionKey };
                tokens.Tokens.Add(refreshToken);
                return await CreateAsync(tokens);
            }

            return await UpdateAsync(tokens, t =>
            {
                t.Tokens.Insert(0, refreshToken);
                if (t.Tokens.Count > 30)
                {
                    //Will remove the item 31, we limit the array as cosmos db document should be small as possible.
                    t.Tokens.RemoveAt(30);
                }

                return t;
            });
        }

        public async Task RevokeAsync(TUser user)
        {
            await DeleteAsync(GetId(user), user.PartitionKey);
        }

        public async Task<RefreshToken> GetTokenAsync(TUser user, string token)
        {
            var query = GetQuery(new QueryOptions() { MaxItemCount = 1, 
                PartitionKey = user.PartitionKey });
            string id = GetId(user);
            query = query.Where(x => x.Id == id && x.Tokens.Any(z => z.Token == token));
            var resultQuery = query.SelectMany(x => x.Tokens);
            return await _context.FirstOrDefaultAsync(resultQuery);
        }
    }
}