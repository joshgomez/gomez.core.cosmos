﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.Core.Cosmos.Models.Areas.Sockets;
using Gomez.Core.DocumentDb;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.Core.Cosmos.Repositories
{
    public interface IConnectionMapRepository<TUser, TRole> : IDocumentRepository<ConnectionMap>
         where TRole : DocumentDbIdentityRole
         where TUser : DocumentDbIdentityUser<TRole>
    {
        Task<int> CountAsync(bool? online);
        Task<int> CountAsync(bool? online, string groupId);
        IAsyncEnumerable<string> GetActiveUsersAsync();
        IAsyncEnumerable<string> GetActiveUsersAsync(string groupId);
        IAsyncEnumerable<string> GetExpiredUsersAsync();
        IAsyncEnumerable<string> GetExpiredUsersAsync(string groupId);
    }
}