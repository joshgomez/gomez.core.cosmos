﻿using Gomez.Core.Cosmos.Models.Areas.Sockets.ConnectionMapModels;
using Gomez.Core.DocumentDb;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Gomez.Core.Cosmos.Models.Areas.Sockets
{
    public class ConnectionMap : Gomez.Core.DocumentDb.DefaultDocumentEntity, 
        IPartitionedDocumentEntity
    {
        public ConnectionMap()
        {
            Connections = new List<ConnectionGroup>();
            Type = nameof(ConnectionMap);
        }

        /// <summary>
        /// The user id (guid)
        /// </summary>
        [JsonProperty("partitionKey")]
        public string PartitionKey { get; set; }

        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("connections")]
        public ICollection<ConnectionGroup> Connections { get; set; }

        [JsonProperty("expireAt")]
        public DateTime ExpireAt { get; set; }
    }
}