﻿using Gomez.Core.Web.Sockets;
using Newtonsoft.Json;

namespace Gomez.Core.Cosmos.Models.Areas.Sockets.ConnectionMapModels
{
    public class ConnectionGroup : IConnectionGroup
    {
        public ConnectionGroup()
        {

        }

        public ConnectionGroup(string connectionId, string groupId)
        {
            ConnectionId = connectionId;
            GroupId = groupId;
        }

        [JsonProperty("connectionId")]
        public string ConnectionId { get; set; }
        
        [JsonProperty("groupId")]
        public string GroupId { get; set; }
    }
}
