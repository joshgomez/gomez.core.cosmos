﻿using Gomez.Core.DocumentDb;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Gomez.Core.Cosmos.Models.Areas.Identity
{
    public class RefreshTokens : Gomez.Core.DocumentDb.DefaultDocumentEntity, IPartitionedDocumentEntity
    {
        [JsonProperty("tokens")]
        public List<RefreshToken> Tokens { get; set; }

        [JsonProperty("partitionKey")]
        public string PartitionKey { get; set; }

        public RefreshTokens()
        {
            Tokens = new List<RefreshToken>();
            Type = this.GetType().Name;
        }

        public RefreshTokens(string id) : this()
        {
            Id = id;
        }
    }
}