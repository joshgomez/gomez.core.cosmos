﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

namespace Gomez.Core.Cosmos.Models.Areas.Identity
{
    public class RefreshToken
    {
        [JsonProperty("token")]
        [MaxLength(255)]
        public string Token { get; set; }

        [JsonProperty("expires")]
        public DateTime Expires { get; set; }

        [JsonProperty("remoteIpAddress")]
        [MaxLength(16)]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>")]
        public byte[] RemoteIpAddress { get; set; }

        [NotMapped]
        [JsonIgnore]
        public bool Active => DateTime.UtcNow <= Expires;

        public RefreshToken()
        {
        }

        public RefreshToken(string token, DateTime expires) : this()
        {
            Token = token;
            Expires = expires;
        }

        public RefreshToken(string token, DateTime expires, IPAddress remoteIpAddress) : this(token, expires)
        {
            RemoteIpAddress = remoteIpAddress.GetAddressBytes();
        }

        public RefreshToken(string token, DateTime expires, byte[] remoteIpAddress) : this(token, expires)
        {
            RemoteIpAddress = remoteIpAddress;
        }
    }
}