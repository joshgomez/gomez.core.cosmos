﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.Core.Utilities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.Core.Cosmos.Models.Contexts
{
    public class ApplicationDbInitializer<TUser, TRole>
        where TRole : DocumentDbIdentityRole, new()
        where TUser : DocumentDbIdentityUser<TRole>, new()
    {
        private readonly List<string> _roles;
        private readonly Dictionary<string, (string password, string[] roles)> _users;
        private readonly RoleManager<TRole> _roleManager;
        private readonly UserManager<TUser> _userManager;

        public ApplicationDbInitializer(UserManager<TUser> userManager, RoleManager<TRole> roleManager)
        {
            _users = new Dictionary<string, (string, string[])>();
            _roles = new List<string>();
            _userManager = userManager;
            _roleManager = roleManager;
        }

        protected void AddUser(string email, string password, params string[] roles)
        {
            _users.Add(email, (password, roles));
        }

        protected void AddRole(string name)
        {
            _roles.Add(name);
        }

        public void SeedUsers()
        {
            if (_users.Count == 0)
            {
                return;
            }

            AsyncPump.Run(async () =>
            {
                await AddUsersAsync();
            });
        }

        private async Task AddUsersAsync()
        {
            foreach (var item in _users)
            {
                await AddUserAsync(item.Key, item.Value.password, item.Value.roles);
            }
        }

        private async Task AddUserAsync(string email, string password, params string[] roles)
        {
            if (await _userManager.FindByNameAsync(email) == null)
            {
                var user = new TUser
                {
                    UserName = email,
                    Email = email
                };

                IdentityResult result = await _userManager.CreateAsync(user, password);
                if (roles.Length > 0 && result.Succeeded)
                {
                    await _userManager.AddToRolesAsync(user, roles);
                }
            }
        }

        public void SeedRoles()
        {
            if (_roles.Count == 0)
            {
                return;
            }

            AsyncPump.Run(async () =>
            {
                await AddRolesAsync();
            });
        }

        private async Task AddRolesAsync()
        {
            foreach (var item in _roles)
            {
                await AddRoleAsync(item);
            }
        }

        private async Task AddRoleAsync(string name)
        {
            if (await _roleManager.FindByNameAsync(name) == null)
            {
                var role = new TRole
                {
                    Name = name
                };

                await _roleManager.CreateAsync(role);
            }
        }
    }
}