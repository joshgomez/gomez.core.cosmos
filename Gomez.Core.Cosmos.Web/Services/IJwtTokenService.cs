﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.Core.Cosmos.Models.Areas.Identity;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;

namespace Gomez.Core.Cosmos.Web.Services
{
    public interface IJwtTokenService<in TRole, in TUser>
        where TRole : DocumentDbIdentityRole
        where TUser : DocumentDbIdentityUser<TRole>
    {
        string BuildToken(TUser user, IEnumerable<Claim> additionalClaims = null);

        RefreshToken GenerateRefreshToken(TUser user, IPAddress ip);

        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    }
}