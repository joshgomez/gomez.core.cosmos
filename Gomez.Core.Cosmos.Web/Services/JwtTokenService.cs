﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.Core.Cosmos.Models.Areas.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Gomez.Core.Cosmos.Web.Services
{
    public abstract class JwtTokenService
    {
        public const string JWT_KEY = "Jwt:Key";
        public const string JWT_ISSUER = "Jwt:Issuer";
        public const string JWT_AUDIENCE = "Jwt:Audience";
        public const string JWT_EXPIRETIME = "Jwt:ExpireTime";
        public const string JWT_REFRESH_EXPIRETIME = "Jwt:RefreshExpireTime";

        protected JwtTokenService()
        {
        }
    }

    public class JwtTokenService<TRole, TUser> : JwtTokenService,
        IJwtTokenService<TRole, TUser>
        where TRole : DocumentDbIdentityRole
        where TUser : DocumentDbIdentityUser<TRole>
    {
        public static Func<TUser, IEnumerable<Claim>> RegisterClaims { get; set; } = null;
        private readonly IConfiguration _config;

        public JwtTokenService(IConfiguration config)
        {
            _config = config;
        }

        /// <summary>
        /// Builds the token used for authentication
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string BuildToken(TUser user, IEnumerable<Claim> additionalClaims = null)
        {
            var expireTime = DateTime.UtcNow.AddMinutes(double.Parse(_config[JWT_EXPIRETIME]));

            // Create a claim based on the users emai. You can add more claims like ID's and any other info
            IEnumerable<Claim> claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };

            if (RegisterClaims != null)
            {
                claims = claims.Union(RegisterClaims(user));
            }

            if (additionalClaims != null)
            {
                claims = claims.Union(additionalClaims);
            }

            // Creates a key from our private key that will be used in the security algorithm next
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config[JWT_KEY]));

            // Credentials that are encrypted which can only be created by our server using the private key
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            // this is the actual token that will be issued to the user
            var token = new JwtSecurityToken(_config[JWT_ISSUER],
                _config[JWT_AUDIENCE],
                claims,
                expires: expireTime,
                signingCredentials: creds);

            // return the token in the correct format using JwtSecurityTokenHandler
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Generate a refresh token to be saved in a store for both the back-end and front-end.
        /// </summary>
        /// <returns></returns>
        public RefreshToken GenerateRefreshToken(TUser user, IPAddress ip)
        {
            var expireTime = DateTime.UtcNow.AddMinutes(double.Parse(_config[JWT_REFRESH_EXPIRETIME]));
            var randomNumber = new byte[32];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomNumber);
            string token = Convert.ToBase64String(randomNumber);
            return new RefreshToken(token, expireTime, ip);
        }

        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config[JWT_KEY])),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            if (!(securityToken is JwtSecurityToken jwtSecurityToken) || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }
    }
}