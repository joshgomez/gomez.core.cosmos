﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.Core.Cosmos.Models.Areas.Sockets;
using Gomez.Core.Cosmos.Models.Areas.Sockets.ConnectionMapModels;
using Gomez.Core.Cosmos.Repositories;
using Gomez.Core.Web.Sockets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.Cosmos.Web.Sockets
{
    public class CosmosConnectionMapping<TModel, TRole, TUser> : IAsyncConnectionMapping<string, TModel>
         where TRole : DocumentDbIdentityRole
         where TUser : DocumentDbIdentityUser<TRole>
    {
        private readonly IConnectionMapRepository<TUser, TRole> _store;
        private readonly int _maxConnections;
        private readonly string _hubName;

        public CosmosConnectionMapping(IConnectionMapRepository<TUser, TRole> store, 
            string hubName,
            int maxConnections = 5)
        {
            _store = store;
            _hubName = hubName;
            _maxConnections = maxConnections;
        }

        private string GetId(string userId)
        {
            return userId + "_cmp";
        }

        private void ExtendExpireDate(ConnectionMap document)
        {
            var currentTime = DateTime.UtcNow;
            document.ExpireAt = currentTime.AddMinutes(5);
        }

        public Task<int> CountAsync(bool? online)
        {
            return _store.CountAsync(online);
        }

        public Task<int> CountAsync(bool? online, string groupId)
        {
            return _store.CountAsync(online, groupId);
        }

        public async Task AddAsync(string userId, string connectionId, TModel data, params string[] groupIds)
        {
            var document = await _store.ReadAsync(GetId(userId), _hubName);
            if (document == null)
            {
                document = new ConnectionMap()
                {
                    Id = GetId(userId),
                    PartitionKey = _hubName,
                };
            }
            else if(DateTime.UtcNow > document.ExpireAt)
            {
                document.Connections.Clear();
            }

            if (document.Connections.Count + 1 > _maxConnections)
            {
                throw new Core.Models.Exceptions.TooManyException();
            }

            await _store.UpsertAsync(document, d =>
            {
                d.Data = JsonConvert.SerializeObject(data);
                if (groupIds == null || groupIds.Length == 0)
                {
                    d.Connections.Add(new ConnectionGroup(connectionId, null));
                }
                else
                {
                    foreach (var groupId in groupIds)
                    {
                        d.Connections.Add(new ConnectionGroup(connectionId, groupId));
                    }
                }

                ExtendExpireDate(d);
                return d;
            });
        }

        public async IAsyncEnumerable<IConnectionGroup> GetConnectionsAsync(string userId)
        {
            var document = await _store.ReadAsync(GetId(userId), _hubName);
            if(document == null || document.Connections == null || document.Connections.Count == 0)
            {
                yield break;
            }

            foreach (var connection in document.Connections)
            {
                yield return connection;
            }
        }

        public async Task<TModel> GetDataAsync(string userId)
        {
            var document = await _store.ReadAsync(GetId(userId), _hubName);
            await _store.UpdateAsync(document, d => { ExtendExpireDate(document); return d; });
            return JsonConvert.DeserializeObject<TModel>(document.Data);
        }

        public async Task<DateTime?> GetExpireDateAsync(string userId)
        {
            var document = await _store.ReadAsync(GetId(userId), _hubName);
            await _store.UpdateAsync(document, d => { ExtendExpireDate(document); return d; });
            return document.ExpireAt;
        }

        public IAsyncEnumerable<string> GetExpireUsersAsync()
        {
            return _store.GetExpiredUsersAsync();
        }

        public IAsyncEnumerable<string> GetExpireUsersAsync(string groupId)
        {
            return _store.GetExpiredUsersAsync(groupId);
        }

        public IAsyncEnumerable<string> GetActiveUsersAsync()
        {
            return _store.GetActiveUsersAsync();
        }

        public IAsyncEnumerable<string> GetActiveUsersAsync(string groupId)
        {
            return _store.GetActiveUsersAsync(groupId);
        }

        public Task RemoveAsync(string userId)
        {
            return RemoveAsync(userId, null);
        }

        public async Task RemoveAsync(string userId, string connectionId)
        {
            if (connectionId == null)
            {
                await _store.DeleteAsync(GetId(userId), _hubName);
                return;
            }

            var document = await _store.ReadAsync(GetId(userId), _hubName);
            if (document == null)
            {
                document = new ConnectionMap()
                {
                    Id = GetId(userId),
                    PartitionKey = _hubName,
                };
            }

            document = await _store.UpsertAsync(document, d =>
            {
                var removedConnections = d.Connections.Where(x => x.ConnectionId == connectionId).ToArray();
                if(removedConnections.Length > 0)
                {
                    foreach (var connection in removedConnections)
                    {
                        d.Connections.Remove(connection);
                    }
                }

                return d;
            });

            if(document.Connections.Count == 0)
            {
                await _store.DeleteAsync(GetId(userId), _hubName);
            }
        }
    }
}