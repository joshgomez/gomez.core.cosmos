﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Gomez.Core.Cosmos.Web.Controllers
{
    public abstract class MainController<TRole, TUser, TController> : Controller
        where TRole : DocumentDbIdentityRole
        where TUser : DocumentDbIdentityUser<TRole>
        where TController : Controller
    {
        protected readonly UserManager<TUser> _userManager;
        protected readonly IStringLocalizerFactory _localizerFactory;
        protected readonly ILogger<TController> _logger;

        protected MainController(IStringLocalizerFactory localizerFactory) : base()
        {
            _localizerFactory = localizerFactory;
        }

        protected MainController(UserManager<TUser> um, IStringLocalizerFactory localizerFactory) : this(localizerFactory)
        {
            _userManager = um;
        }

        protected MainController(
            UserManager<TUser> um,
            IStringLocalizerFactory localizerFactory,
            ILogger<TController> logger) : this(um, localizerFactory)
        {
            _logger = logger;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var requestCulture = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            ViewData["Language"] = requestCulture.RequestCulture.UICulture.TwoLetterISOLanguageName;
            base.OnActionExecuting(context);
        }
    }
}