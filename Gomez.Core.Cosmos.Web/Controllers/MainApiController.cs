﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Gomez.Core.Cosmos.Web.Controllers
{
    [ApiController]
    public abstract class MainApiController<TRole, TUser, TController> : ControllerBase
        where TRole : DocumentDbIdentityRole
        where TUser : DocumentDbIdentityUser<TRole>
        where TController : ControllerBase
    {
        protected readonly UserManager<TUser> _userManager;
        protected readonly IStringLocalizerFactory _localizerFactory;
        protected readonly ILogger<TController> _logger;

        protected MainApiController(IStringLocalizerFactory localizerFactory) : base()
        {
            _localizerFactory = localizerFactory;
        }

        protected MainApiController(UserManager<TUser> um, IStringLocalizerFactory localizerFactory) : this(localizerFactory)
        {
            _userManager = um;
        }

        protected MainApiController(
            UserManager<TUser> um,
            IStringLocalizerFactory localizerFactory,
            ILogger<TController> logger) : this(um, localizerFactory)
        {
            _logger = logger;
        }
    }
}