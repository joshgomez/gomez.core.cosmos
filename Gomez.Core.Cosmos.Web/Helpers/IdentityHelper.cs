﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Gomez.Core.Cosmos.Web.Helpers
{
    public static class IdentityHelper
    {
        public static async Task<IList<Claim>> GetValidClaimsAsync<TUser, TRole>(TUser user,
            UserManager<TUser> userManager, RoleManager<TRole> roleManager)
            where TRole : DocumentDbIdentityRole
            where TUser : DocumentDbIdentityUser<TRole>
        {
            var claims = new List<Claim>();
            var userClaims = await userManager.GetClaimsAsync(user);
            var userRoles = await userManager.GetRolesAsync(user);
            claims.AddRange(userClaims);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                var role = await roleManager.FindByNameAsync(userRole);
                if (role != null)
                {
                    var roleClaims = await roleManager.GetClaimsAsync(role);
                    foreach (Claim roleClaim in roleClaims)
                    {
                        claims.Add(roleClaim);
                    }
                }
            }

            return claims;
        }
    }
}