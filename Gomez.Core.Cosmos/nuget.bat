@echo off
cd %~dp0
nuget.exe pack Gomez.Core.Cosmos.csproj -IncludeReferencedProjects -Properties Configuration=Release -OutputDirectory "D:\NuGetPackages"